# Dashboard Analytic

## Setup

  

``` bash
# Clone Code from repository
$ git clone http://gitlab.playcourt.id/dtp-dsa/frontend-services/dashboard-analytic-frontend.git

#Move to project directory
$ cd dashboard-analytic-frontend

# install dependencies
$ npm install # Or yarn install
```

  

## Usage

  

### Development

  

``` bash
# serve with hot reloading at localhost:8080
$ npm run start:development
```

  

Go to [http://localhost:8080](http://localhost:8080)

  

### Production

  

``` bash
# build for production and launch the server
$ npm run start:production
```

  

### Test

``` bash
# run unit test
$ npm run test
```
### Built With
* Node JS v12 - Javascript Version
* Vue JS 2.6.11

### Authors
* Ronaldo Cristover

### License
For internal purpose only